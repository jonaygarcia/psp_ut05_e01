# Criptografía asimétrica

La criptografía asimétrica (en inglés asymmetric key cryptography), también llamada criptografía de clave pública (en inglés public key cryptography) o criptografía de dos claves ​(en inglés two-key cryptography), es el método criptográfico que usa un par de claves para el envío de mensajes. Las dos claves pertenecen a la misma persona que ha enviado el mensaje. Una clave es pública y se puede entregar a cualquier persona, la otra clave es privada y el propietario debe guardarla de modo que nadie tenga acceso a ella. Además, los métodos criptográficos garantizan que esa pareja de claves sólo se puede generar una vez, de modo que se puede asumir que no es posible que dos personas hayan obtenido casualmente la misma pareja de claves.

Si el remitente usa la clave pública del destinatario para cifrar el mensaje, una vez cifrado, sólo la clave privada del destinatario podrá descifrar este mensaje, ya que es el único que la conoce. Por tanto se logra la confidencialidad del envío del mensaje, nadie salvo el destinatario puede descifrarlo.

Si el propietario del par de claves usa su clave privada para cifrar el mensaje, cualquiera puede descifrarlo utilizando su clave pública. En este caso se consigue por tanto la identificación y autentificación del remitente, ya que se sabe que sólo pudo haber sido él quien empleó su clave privada (salvo que alguien se la hubiese podido robar). Esta idea es el fundamento de la firma electrónica.

Los __sistemas de cifrado de clave pública__ o __sistemas de cifrado asimétricos__ se inventaron con el fin de evitar por completo el problema del intercambio de claves de los sistemas de cifrado simétricos. Con las claves públicas no es necesario que el remitente y el destinatario se pongan de acuerdo en la clave a emplear. Todo lo que se requiere es que, antes de iniciar la comunicación secreta, el remitente consiga una copia de la clave pública del destinatario. Es más, esa misma clave pública puede ser usada por cualquiera que desee comunicarse con su propietario. Por tanto, se necesitarán sólo n pares de claves por cada n personas que deseen comunicarse entre sí.

## Descripción

Las dos principales ramas de la criptografía de clave pública son:

* __Cifrado de clave pública__: un mensaje cifrado con la clave pública de un destinatario no puede ser descifrado por nadie (incluyendo al que lo cifró), excepto un poseedor de la clave privada correspondiente, presumiblemete su propietario y la persona asociada con la clave pública utilizada. Su función es garantizar la confidencialidad del mensaje.

* __Firmas digitales__: un mensaje firmado con la clave privada del remitente puede ser verificado por cualquier persona que tenga acceso a la clave pública de dicho remitente, lo que demuestra que este remitente tenía acceso a la clave privada (y por lo tanto, es probable que sea la persona asociada con la clave pública utilizada). Se asegura así que el mensaje no ha sido alterado, puesto que cualquier manipulación del mensaje repercutiría en un distinto resultado del algoritmo de resumen del mensaje (encoded message digest). Se utiliza para garantizar la autenticidad del mensaje.

Una analogía con el cifrado de clave pública es la de un buzón con una ranura de correo. La ranura de correo está expuesta y accesible al público; su ubicación (la dirección de la calle) es, en esencia, la clave pública. Alguien que conozca la dirección de la calle puede ir a la puerta y colocar un mensaje escrito a través de la ranura; sin embargo, sólo la persona que posee la llave (clave privada) puede abrir el buzón de correo y leer el mensaje.

Una analogía para firmas digitales es el sellado de un sobre con un sello personal. El mensaje puede ser abierto por cualquier persona, pero la presencia del sello autentifica al remitente.


### Ejemplo de cifrado de mensaje: Ana envía un mensaje a David

![img_01][img_01]

1. Ana redacta un mensaje.
2. Ana cifra el mensaje con la clave pública de David.
3. Ana envía el mensaje cifrado a David a través de internet, ya sea por correo electrónico, mensajería instantánea o cualquier otro medio.
4. David recibe el mensaje cifrado y lo descifra con su clave privada.
5. David ya puede leer el mensaje original que le mandó Ana.

### Ejemplo de firma digital con clave asimétrica: David envía un mensaje a Ana

![img_02][img_02]

1. David redacta un mensaje.
2. David firma digitalmente el mensaje con su clave privada.
3, David envía el mensaje firmado digitalmente a Ana a través de internet, ya sea por correo electrónico, mensajería instantánea o cualquier otro medio.
4. Ana recibe el mensaje firmado digitalmente y comprueba su autenticidad usando la clave pública de David.
5. Ana ya puede leer el mensaje con total seguridad de que ha sido David el remitente.

## Esquemas para la propagación de la confianza

Observar que la criptografía de clave pública necesita establecer una confianza en que la clave pública de un usuario (al cual se identifica por una cadena identificativa a la que se llama identidad) es correcta, es decir el único que posee la clave privada correspondiente es el usuario auténtico al que pertenece. Cuanto más fiable sea el método más seguridad tendrá el sistema.

Lo ideal sería que cada usuario comunicara (e idealmente probara) de forma directa al resto de usuarios cual es su clave pública. Sin embargo esto no es posible en la realidad y se desarrollan distintos esquemas para aportar confianza. Estos esquemas se pueden agrupar en dos tipos: Esquema centralizados y esquemas descentralizados. En los esquemas descentralizado hay varios nodos y cada uno tiene unas capacidades y derechos. En los esquemas centralizados hay una arquitectura cliente-servidor donde los servidores juegan un papel central y proveen servicios a los clientes. Cada esquema tiene sus ventajas e inconvenientes por ejemplo, los sistemas centralizados suelen ser más vulnerables a ataques de denegación de servicio debido a que basta con que falle el servidor central para que el sistema de confianza caiga por completo. Los sistemas descentralizados se suelen considerar menos seguros contra ataques encaminados a publicar claves públicas falsas debido a que al haber varios nodos posibles a atacar es más difícil asegurar su seguridad. Los modelos más usados son:

* __Uso de una infraestructura de clave pública o PKI__. En este modelo hay una o varias entidades emisoras de certificados (Autoridades de certificación o CA del inglés Certification Authority) que aseguran la autenticidad de la clave pública y de ciertos atributos del usuario. Para ello firman con su clave privada ciertos atributos del usuario incluyendo su clave pública generando lo que se llama certificado del usuario.

* __Establecimiento de una red de confianza__. No hay nodos aparte de los usuarios. Los usuarios recogen claves públicas de otros usuarios y aseguran su autenticidad si están seguros de que la clave privada correspondiente pertenece en exclusiva a ese usuario. Un usuario además puede directamente confiar en el conjunto de claves públicas en las que otro confía ya sea directamente o a través de otras relaciones de confianza. En cada caso es el propio usuario el que decide el conjunto de claves públicas en las que confía y su grado de fiabilidad. Dos usuarios que no se conocen pueden confiar en sus claves públicas si existe una cadena de confianza que enlace ambas partes. Este tipo de implementación de la confianza es el que usa por ejemplo PGP.

* __Uso de criptografía basada en identidad__. En este modelo existe un generador de claves privadas o PKG (acrónimo de Private Key Generator) que a partir de una cadena de identificación del usuario genera una clave privada y otra pública para ese usuario. La pública la difunde para que el resto de usuarios la sepan y la privada es comunicada en exclusiva al usuario a quien pertenece.

* __Uso de criptografía basada en certificados__. En este modelo el usuario posee una clave privada y otra pública. La clave pública la envía a una Autoridad de certificación que basándose en criptografía basada en identidad genera un certificado que asegura la validez de los datos.

* __Uso de criptografía sin certificados__. Este modelo es similar al modelo que usa criptografía basada en identidad pero con la diferencia de que lo que se genera en el centro generador de claves o KGC (acrónimo de Key Generator Center) es una clave parcial. La clave privada completa se genera a partir de la clave privada parcial y un valor generado aleatoriamente por el usuario. La clave pública es generada también por el usuario a partir de parámetros públicos del KGC y el valor secreto escogido.

__Girault__ distingue tres niveles de confianza que dan los distintos modelos a la autoridad que interviene en el proceso (PKG, KGC o CA según cada caso):

* __Nivel 1__: La autoridad puede calcular claves secretas de usuarios y por tanto pueden hacerse pasar como cualquier usuario sin ser detectado. Las firmas basadas en identidad pertenecen a este nivel de confianza.

* __Nivel 2__: La autoridad no puede calcular claves secretas de usuarios, pero puede todavía hacerse pasar como cualquier usuario sin ser detectado. Firmas sin certificados pertenecen a este nivel.

* __Nivel 3__: La autoridad no puede calcular claves secretas de usuarios, y tampoco puede hacerse pasar como un usuario sin ser detectado. Es el nivel más alto de fiabilidad. Las firmas tradicionales PKI y la firmas basadas en certificados pertenecen a este nivel.

## Seguridad

Según el segundo principio de Kerckhoffs toda la seguridad debe descansar en la clave y no en el algoritmo (en contraposición con la seguridad por la oscuridad). Por lo tanto, el tamaño de la clave es una medida de la seguridad del sistema, pero no se puede comparar el tamaño de la clave del cifrado simétrico con el del cifrado de clave pública para medir la seguridad. En un ataque de fuerza bruta sobre un cifrado simétrico con una clave del tamaño de 80 bits, el atacante debe probar hasta 280-1 claves para encontrar la clave correcta. En un ataque de fuerza bruta sobre un cifrado de clave pública con una clave del tamaño de 512 bits, el atacante debe factorizar un número compuesto codificado en 512 bits (hasta 155 dígitos decimales). La cantidad de trabajo para el atacante será diferente dependiendo del cifrado que esté atacando. Mientras 128 bits son suficientes para cifrados simétricos, dada la tecnología de factorización de hoy en día, se recomienda el uso de claves públicas de 1024 bits para la mayoría de los casos.

### Ventajas y desventajas del cifrado asimétrico

La mayor ventaja de la criptografía asimétrica es que la distribución de claves es más fácil y segura ya que la clave que se distribuye es la pública manteniéndose la privada para el uso exclusivo del propietario, pero este sistema tiene bastantes desventajas:

* Para una misma longitud de clave y mensaje se necesita mayor tiempo de proceso.
* Las claves deben ser de mayor tamaño que las simétricas (generalmente son cinco o más veces de mayor tamaño que las claves simétricas).
* El mensaje cifrado ocupa más espacio que el original.

Los nuevos sistemas de clave asimétrica basado en curvas elípticas tienen características menos costosas.

Herramientas como PGP, SSH o la capa de seguridad SSL para la jerarquía de protocolos TCP/IP utilizan un híbrido formado por la criptografía asimétrica para intercambiar claves de criptografía simétrica, y la criptografía simétrica para la transmisión de la información.

Algunos algoritmos y tecnologías de clave asimétrica son:

* Diffie-Hellman
* RSA
* DSA
* ElGamal
* Criptografía de curva elíptica
* Criptosistema de Merkle-Hellman
* Goldwasser-Micali
* Goldwasser-Micali-Rivest
* Cifrado extremo a Extremo

Algunos protocolos que usan los algoritmos antes citados son:

* DSS ("Digital Signature Standard") con el algoritmo DSA ("Digital Signature Algorithm")
* PGP
* GPG, una implementación de OpenPGP
* SSH
* SSL, ahora un estándar del IETF
* TLS


[img_01]: img/01.png "Criptografia Asimetrica"
[img_02]: img/02.png "Firma Digital Asimétrica"

## Algoritmo RSA

El sistema criptográfico con clave pública RSA es un algoritmo asimétrico cifrador de bloques, que utiliza una clave pública, la cual se distribuye (en forma autenticada preferentemente), y otra privada, la cual es guardada en secreto por su propietario.

Una clave es un número de gran tamaño, que una persona puede conceptualizar como un mensaje digital, como un archivo binario o como una cadena de bits o bytes.

Cuando se envía un mensaje, el emisor busca la clave pública de cifrado del receptor y una vez que dicho mensaje llega al receptor, éste se ocupa de descifrarlo usando su clave oculta.

Los mensajes enviados usando el algoritmo RSA se representan mediante números y el funcionamiento se basa en el producto de dos números primos grandes (mayores que 10<sup>100</sup>) elegidos al azar para conformar la clave de descifrado.

Emplea expresiones exponenciales en aritmética modular.

La seguridad de este algoritmo radica en que no hay maneras rápidas conocidas de factorizar un número grande en sus factores primos utilizando computadoras tradicionales.
La computación cuántica podría proveer una solución a este problema de factorización.

El algoritmo RSA es un algoritmo de clave pública desarrollado en 1977 en el MIT por Ronald Rivest, Adi Shamir y Leonard Adelman.

Fue registrado el 20 de Septiembre de 1983. El 20 de Septiembre del 2000, tras 17 años, expiró la patente RSA, pasando a ser un algoritmo de dominio público.

Este popular sistema se basa en el problema matemático de la factorización de numeros grandes.


### Ejemplo java de RSA

El siguiente código genera unas claves pública y privada RSA de 2048 bits que usa para encriptar y desencriptar un mensaje:

```java
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class EncryptedMessage {

    public static void main(String [] args) throws Exception {

    	String message = "This is a secret message";

        // generate public and private keys
        KeyPair keyPair = buildKeyPair();
        PublicKey pubKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        // original message
        System.out.print("Mensaje original: ");
        System.out.println(message);  // <<encrypted message>>

        // encrypt the message
        byte [] encrypted = encrypt(privateKey, message);
        System.out.print("Mensaje encriptado: ");
        System.out.println(new String(encrypted));  // <<encrypted message>>

        // decrypt the message
        byte[] secret = decrypt(pubKey, encrypted);
        System.out.print("Mensaje desencriptado: ");
        System.out.println(new String(secret));     // This is a secret message

    }

    public static KeyPair buildKeyPair() throws NoSuchAlgorithmException {
        final int keySize = 2048;
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);
        return keyPairGenerator.genKeyPair();
    }

    public static byte[] encrypt(PrivateKey privateKey, String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);

        return cipher.doFinal(message.getBytes());
    }

    public static byte[] decrypt(PublicKey publicKey, byte [] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);

        return cipher.doFinal(encrypted);
    }
}
```
